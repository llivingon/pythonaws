#!/usr/bin/env python
#
# readS3Object.py
#
# A simple Python script that uses the Boto3 (https://aws.amazon.com/)
# and reads an S3 object from an AWS account using assume role
#

import boto3
import gzip
import shutil
from io import BytesIO

AWS_REGION = 'eu-west-2'
AWS_ROLE_ARN = 'role/test-admin/test-user'
HOST_ENVIRONMENT = 'Development'

def getAccountIDs(awsEnvironment):
    environmentDict = {
                       "Development" :    {"AccountId1" : 111111111111, "AccountId2": 222222222222, "AccountId3": 33333333333},
                       "Production"     : {"AccountId1" : 444444444444, "AccountId2": 555555555555, "AccountId3": 66666666666}
                       }
    return environmentDict[awsEnvironment]

def getAssumeRoleSession(accountId):
    assumeRoleClient = boto3.client('sts')
    sts_response = assumeRoleClient.assume_role(
        RoleArn='arn:aws:iam::%s:%s'%(accountId, AWS_ROLE_ARN),
        RoleSessionName='boto3-session',
        DurationSeconds=900,
    )
    credentials = sts_response['Credentials']
    credentialsDict = {
         'aws_access_key_id': credentials['AccessKeyId'],
         'aws_secret_access_key': credentials['SecretAccessKey'],
         'aws_session_token': credentials['SessionToken'],
     }
    return credentialsDict

def getAwsClient(awsComponentName, vpcName, awsEnvironment=HOST_ENVIRONMENT):
    return boto3.client(
    service_name=awsComponentName,
    region_name=AWS_REGION,
    **getAssumeRoleSession(getAccountIDs(awsEnvironment)[vpcName])
)

def getAwsResource(awsComponentName, vpcName, awsEnvironment=HOST_ENVIRONMENT):
    return boto3.resource(
    service_name=awsComponentName,
    region_name=AWS_REGION,
    **getAssumeRoleSession(getAccountIDs(awsEnvironment)[vpcName])
)

def readGzipS3Obj(bucket, key, fileInMemory):
    compressed_fp = BytesIO()
    bucket.download_fileobj(key, compressed_fp)
    compressed_fp.seek(0)
    with gzip.GzipFile(fileobj=compressed_fp, mode='rb') as gz:
        shutil.copyfileobj(gz, fileInMemory)
        
s3MgmtResource = getAwsResource('s3', 'AccountId1')
s3MgmtClient = getAwsClient('s3', 'AccountId1')

#Read Object stored as a plaintext
bucket =s3MgmtResource.Bucket('s3-bucket-name-plaintext')
prefix ='s3-objecy-prefix-plaintext'
objectReferences =  s3MgmtClient.list_objects_v2(Prefix=prefix, MaxKeys=50, Bucket='s3-bucket-name-plaintext')
objectReferences['Contents'] = list(reversed(objectReferences['Contents'])) #To get latest objects first
for content in objectReferences['Contents']:
    print "\Reading Contents for Object: " + content['Key']
    s3object = s3MgmtResource.Object('s3-bucket-name-plaintext',content['Key'])
    response = s3object.get()['Body'].read()
    print response

#Read Object stored as a GZip
bucket =s3MgmtResource.Bucket('s3-bucket-name-gzipped')
prefix ='s3-objecy-prefix-gzipped'
objectReferences =  s3MgmtClient.list_objects_v2(Prefix=prefix, MaxKeys=50, Bucket='s3-bucket-name-gzipped')
objectReferences['Contents'] = list(reversed(objectReferences['Contents'])) #To get latest objects first
   
for content in objectReferences['Contents']:
    print "\Unzipping and Reading Contents for Object: " + content['Key']
    with open(str(content['Key'])[-43:], 'wb') as fileInMemory: #Sometimes filename can be very long, so trimming filename to ending 43 characters
        readGzipS3Obj(bucket, content['Key'], fileInMemory)
    response = open(str(content['Key'])[-43:],'r').read()
    print response